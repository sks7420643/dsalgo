package arrays;

public class ArrayUtils {

    public Integer[] createArray(){
        Integer[] numArray = {4,6,8,9,7,2};
        return numArray;
    }

    public void printArray(Integer[] arr,String message){
        System.out.println(message);
        for (Integer a:arr){
            System.out.print(a);
        }
        System.out.println("");
    }

    public Integer[] removeEvenNumbers(Integer[] arr){
        Integer oddCount=0;
        for (Integer a:arr){
            if (a%2 != 0){
                oddCount++;
            }
        }

        Integer[] oddArray = new Integer[oddCount];

        Integer idx = 0;
        for (int i = 0; i<arr.length;i++){
            if (arr[i]%2 != 0){
                oddArray[idx] = arr[i];
                idx++;
            }
        }
        return oddArray;
    }

    public Integer[] reverseArray(Integer[] arr){
        Integer[] tempArray = new Integer[arr.length];
        Integer end=arr.length-1;
        for (int i=0;i<arr.length;i++){
            tempArray[i] = arr[end];
            end--;
        }
        return tempArray;
    }

    public Integer findMinValue(Integer[] arr){
        Integer min=Integer.MAX_VALUE;

        for (Integer a : arr){
            if (a<min){
                min=a;
            }
        }
        return min;
    }

    public Integer findSecondMAxValue(Integer[] arr){
        Integer max=Integer.MIN_VALUE;
        Integer secondMax=Integer.MIN_VALUE;

        for (Integer a: arr){
            if (a>max){
                secondMax=max;
                max=a;

            }
        }
        return secondMax;
    }
}
