package linkedlist.singly;

public class ReverseLinkedList {
    private ListNode head;
    private static class ListNode{
        private int data;
        private ListNode next;

        public ListNode(int data){
            this.data=data;
            this.next=null;
        }
    }

    public static void main(String[] args) {
        ReverseLinkedList sll=new ReverseLinkedList();
        sll.head=new ListNode(10);
        ListNode second=new ListNode(5);
        ListNode third=new ListNode(8);
        ListNode fourth=new ListNode(1);

        sll.head.next=second;
        second.next=third;
        third.next=fourth;

        sll.display(sll.head);
        ListNode reverseNode=sll.reverseList();
        sll.display(reverseNode);
    }

    private ListNode reverseList() {
        ListNode current=head;
        ListNode previous=null;
        ListNode next=null;

        while (current!=null){
            next=current.next;
            current.next=previous;
            previous=current;
            current=next;
        }
        return previous;
    }


    private void display(ListNode head) {
        System.out.println("start print");
        ListNode current= head;
        while (current!=null){
            System.out.println(current.data);
            current=current.next;
        }
        System.out.println();
    }
}
