package linkedlist.singly;

public class DeleteNodeLinkedList {
    private ListNode head;
    private static class ListNode{
        private int data;
        private ListNode next;

        public ListNode(int data){
            this.data=data;
            this.next=null;
        }
    }

    public static void main(String[] args) throws IllegalAccessException {
        DeleteNodeLinkedList deleteNodeLinkedList =new DeleteNodeLinkedList();
        deleteNodeLinkedList.head=new ListNode(10);
        ListNode second=new ListNode(20);
        ListNode third=new ListNode(30);
        ListNode fourth=new ListNode(40);
        ListNode fifth=new ListNode(50);
        ListNode sixth=new ListNode(60);
        ListNode seventh=new ListNode(70);

        deleteNodeLinkedList.head.next=second;
        second.next=third;
        third.next=fourth;
        fourth.next=fifth;
        fifth.next=sixth;
        sixth.next=seventh;

        deleteNodeLinkedList.display("created node");
//        deleteNodeSinglyLinked.deleteFirstNode();
//        deleteNodeSinglyLinked.deleteLastNode();
        deleteNodeLinkedList.deleteSpecificPositionNode(4);

    }

    private void deleteSpecificPositionNode(int position) {
        if (position==1){
            head=head.next;
            display("deleted first position node");
        }else {
            ListNode current=head;
            ListNode after=null;
            int count=1;

            while (count<position-1){
                count++;
                current=current.next;
            }
            after=current.next.next;
            current.next=after;

            display("deleted position node");
        }
    }

    private ListNode deleteFirstNode() {
        if (head==null || head.next==null){
            return head;
        }
        ListNode current=head;
        head=head.next;
        current.next=null;
        display("delete first node");
        return current;
    }


    private ListNode deleteLastNode() throws IllegalAccessException {
        if (head==null || head.next==null){
            throw new IllegalAccessException("there has no such element");
        }
        ListNode current=head;
        ListNode previous=null;

        while (current.next!=null){
            previous=current;
            current=current.next;
        }
        previous.next=null;
        display("deleted last node");
        return current;

    }

    private void display(String message) {
        ListNode current=head;
        System.out.println(message);
        while (current!=null){
            System.out.println(current.data);
            current=current.next;
        }
        System.out.println();
    }
}
