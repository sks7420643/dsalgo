package linkedlist.singly;

//find nth node from the last

public class FindNthNode {
    ListNode head;
    private static class ListNode{
        private int data;
        private ListNode next;

        public ListNode(int data){
            this.data=data;
            this.next=null;
        }
    }

    public static void main(String[] args) {
        FindNthNode findNthNode=new FindNthNode();
        findNthNode.head=new ListNode(5);
        ListNode second=new ListNode(10);
        ListNode third=new ListNode(15);
        ListNode fourth=new ListNode(20);

        findNthNode.head.next=second;
        second.next=third;
        third.next=fourth;

        findNthNode.display();
        findNthNode.findNode(3);
    }

    private void findNode(int position) {
        if (head==null){
            return;
        }

        if (position<=0){
            throw new IllegalArgumentException("invalid position value:"+position);
        }
        ListNode mainPtr=head;
        ListNode refPtr=head;
        int count=0;
        while (count<position){
            if (refPtr==null){
                throw new IllegalArgumentException(position+" is greater than the number of node in list");
            }
            refPtr=refPtr.next;
            count++;
        }

        while (refPtr!=null){
            mainPtr=mainPtr.next;
            refPtr=refPtr.next;
        }
        System.out.println("nth node:"+mainPtr.data);
    }

    private void display() {
        ListNode current=head;
        while (current!=null){
            System.out.println(current.data);
            current=current.next;
        }
    }
}
